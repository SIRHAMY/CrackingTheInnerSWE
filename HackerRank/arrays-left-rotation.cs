using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution {

    // Complete the rotLeft function below.
    // HG: I believe this runs in O(n) space and time
    static int[] rotLeft(int[] a, int d) {
        // create new array and read into it from d
        int[] rotatedArray = new int[a.Length];
        
        // get vals from d to the end
        // get vals from beginning to d
        
        // (currentPosFromEnd + d) % length, insert at position from end
        for(var positionFromEnd = 0; positionFromEnd < a.Length; positionFromEnd++) {
            var currentIndex = a.Length - 1 - positionFromEnd;
            var positionToMoveTo = rotatedArray.Length - ((positionFromEnd + d) % rotatedArray.Length) - 1;
            
            rotatedArray[positionToMoveTo] = a[currentIndex];
        }
        
        return rotatedArray;
    }

    static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string[] nd = Console.ReadLine().Split(' ');

        int n = Convert.ToInt32(nd[0]);

        int d = Convert.ToInt32(nd[1]);

        int[] a = Array.ConvertAll(Console.ReadLine().Split(' '), aTemp => Convert.ToInt32(aTemp))
        ;
        int[] result = rotLeft(a, d);

        textWriter.WriteLine(string.Join(" ", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
