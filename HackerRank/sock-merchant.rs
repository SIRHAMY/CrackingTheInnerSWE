use std::io;
use std::collections::HashMap;

fn main() {
    let mut number_of_socks = String::new();
    io::stdin().read_line(&mut number_of_socks).expect("something went wrong");
    
    let mut sock_types_input = String::new();
    io::stdin().read_line(&mut sock_types_input).expect("something went wrong");
    
    let sock_types_iterable = sock_types_input.split(" ");
    
    let mut sock_types_counter: HashMap<String, i32> = HashMap::new();
    for sock_type in sock_types_iterable {
        if sock_types_counter.contains_key(sock_type) {
            let current_sock_count = sock_types_counter.get(sock_type).cloned();
            sock_types_counter.insert(String::from(sock_type), current_sock_count.unwrap() + 1);
        } else {
            sock_types_counter.insert(String::from(sock_type), 1);
        }
    }
    
    // Iterate over the hashmap, adding % 2 for each
    let mut total_pairs = 0;
    for sock_number in sock_types_counter.values() {
        // This works because Rust does integer division which auto-floors
        total_pairs = total_pairs + (sock_number / 2);
    }
    println!("{}", total_pairs);
}