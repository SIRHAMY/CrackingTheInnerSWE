using System;
using System.Linq;
using System.Collections.Generic;
					
public class Program
{
	public static void Main()
	{
		var testCases = new List<List<Tuple<int,int>>>();
		testCases.Add(CreateSortedCaseNoOverlap());
		testCases.Add(CreateSortedCaseOverlap());
		testCases.Add(CreateUnSortedCaseNoOverlap());
		testCases.Add(CreateUnSortedCaseOverlap());
		
		for(var testIndex = 0; testIndex < testCases.Count; testIndex++) {
			var testCaseInput = testCases[testIndex];
			var result = MergeMeetings(testCaseInput.ToArray());
			var verificationResult = VerifyMeetingTimesMergedCorrectly(result);
			Console.WriteLine(verificationResult);
		}
	}
	
	// testCases section
	
	public static List<Tuple<int,int>> CreateSortedCaseNoOverlap() {
		return new List<Tuple<int,int>> {
			Tuple.Create(0, 1),
			Tuple.Create(2,3),
			Tuple.Create(4,7),
			Tuple.Create(9, 12)
		};
	}
	
	public static List<Tuple<int,int>> CreateSortedCaseOverlap() {
		return new List<Tuple<int,int>> {
			Tuple.Create(0, 2),
			Tuple.Create(2,5),
			Tuple.Create(4,7),
			Tuple.Create(9, 12)
		};
	}
	
	public static List<Tuple<int,int>> CreateUnSortedCaseOverlap() {
		return new List<Tuple<int,int>> {
			Tuple.Create(4, 7),
			Tuple.Create(2,5),
			Tuple.Create(8, 15),
			Tuple.Create(9, 12)
		};
	}
	
	public static List<Tuple<int,int>> CreateUnSortedCaseNoOverlap() {
		return new List<Tuple<int,int>> {
			Tuple.Create(9, 12),
			Tuple.Create(2,5),
			Tuple.Create(20, 25)
		};
	}
	
	// bizLogic
	
	public static Tuple<int,int>[] MergeMeetings(Tuple<int,int>[] meetingTimes) {
		// HG: Yeah this isn't the fastest, so shoot me. C# didn't implement Tuple list creation from tuple array
		var myNewList = new List<Tuple<int,int>>();
		myNewList.AddRange(meetingTimes);
		var sortedMeetingTimes = myNewList.OrderBy(t => t.Item1).ToList();
		
		var collapsedMeetingTimes = new List<Tuple<int,int>>();
		collapsedMeetingTimes.Add(meetingTimes[0]);
		for(var index = 1; index < meetingTimes.Length; index++) {
			var meetingToCollapseOrAdd = sortedMeetingTimes[index];
			if(ShouldMeetingTimesMerge(collapsedMeetingTimes.Last(), meetingToCollapseOrAdd)) {
				var mergedMeeting = MergeTwoMeetings(collapsedMeetingTimes.Last(), meetingToCollapseOrAdd);
				collapsedMeetingTimes.RemoveAt(collapsedMeetingTimes.Count - 1);
				collapsedMeetingTimes.Add(mergedMeeting);
			} else {
				collapsedMeetingTimes.Add(meetingToCollapseOrAdd);
			}
		}
		
		return collapsedMeetingTimes.ToArray();
	}
	
	public static Tuple<int,int> MergeTwoMeetings(Tuple<int,int> firstMeeting, Tuple<int,int> secondMeeting) {
		var lowerBound = firstMeeting.Item1 < secondMeeting.Item1
			? firstMeeting.Item1
			: secondMeeting.Item1;
		var upperBound = firstMeeting.Item2 > secondMeeting.Item2
			? firstMeeting.Item2
			: secondMeeting.Item2;
		return Tuple.Create(lowerBound, upperBound);
	}
	
	public static bool VerifyMeetingTimesMergedCorrectly(Tuple<int,int>[] mergedMeetingTimes) {
		if(mergedMeetingTimes.Length < 2) {
			return true;
		}
		
		for(var index = 1; index < mergedMeetingTimes.Length; index++) {
			if(ShouldMeetingTimesMerge(mergedMeetingTimes[index - 1], mergedMeetingTimes[index])) {
				return false;
			}
		}
		return true;
	}
	
	public static bool ShouldMeetingTimesMerge(Tuple<int,int> firstMeetingTime, Tuple<int,int> secondMeetingTime) {
		return secondMeetingTime.Item1 <= firstMeetingTime.Item2
			&& secondMeetingTime.Item2 >= firstMeetingTime.Item1;
	}
}