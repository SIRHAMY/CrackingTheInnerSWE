fn main() {
    let my_string = String::from("IAmAstring");
    let max_palindrome_size = longest_palindrome(my_string);
    println!("Max palindrome size {}", max_palindrome_size);
}

// hamytodo: needs to return the string of the longest_palindrome
fn longest_palindrome(s: String) -> usize {
    let mut max_palindrome_size = 0;
    let mut string_index: usize = 0;
    let string_chars:Vec<char> = s.chars().collect();

    while string_index < string_chars.len() {
        let found_palindrome_size = find_palindrome_size(
            &string_chars,
            string_index, 
            string_index + 1);
        if found_palindrome_size > max_palindrome_size {
            max_palindrome_size = found_palindrome_size;
        }

        if string_index < string_chars.len() - 1
            && string_chars[string_index] == string_chars[string_index + 1] {
            let second_found_palindrome_size = find_palindrome_size(
                &string_chars,
                string_index, 
                string_index + 2);
            if second_found_palindrome_size > max_palindrome_size {
                max_palindrome_size = second_found_palindrome_size;
            }
        }

        string_index = string_index + 1;
    }

    return max_palindrome_size;
}

fn find_palindrome_size(
    string_chars: &Vec<char>,
    start_index: usize,
    end_index: usize) -> usize {
    // hamy: we use an i32 for start_index to avoid
    // panics caused by overflowing to -1
    let mut start_pointer = start_index;
    let mut end_pointer_exclusive = end_index;
    let mut palindrome_size = 0;
    while start_pointer > 0
        && end_pointer_exclusive <= string_chars.len()
        && string_chars[start_pointer] == string_chars[end_pointer_exclusive - 1] {
        palindrome_size = end_pointer_exclusive - start_pointer;
        start_pointer = start_pointer - 1;
        end_pointer_exclusive = end_pointer_exclusive + 1;
    }

    return palindrome_size;
}
