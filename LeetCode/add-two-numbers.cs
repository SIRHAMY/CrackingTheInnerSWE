/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int x) { val = x; }
 * }
 */

 // hamy - currently fails on super duper big numbers like
 //[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1]
//[5,6,4]
public class Solution {
    const int Base = 10;
    
    public ListNode AddTwoNumbers(ListNode l1, ListNode l2) {       
        var total = GetListValueFromNode(l1) + GetListValueFromNode(l2);
        Console.WriteLine(total);
        return CreateListFromValue(total);
    }
    
    public ulong GetListValueFromNode(ListNode node) {
        var placeHolder = 0;
        ulong value = 0;
        var currentNode = node;
        
        while(currentNode != null) {
            value = value + (ulong)currentNode.val * (ulong)Math.Pow(Base, placeHolder);
            placeHolder++;
            currentNode = currentNode.next;
        }
        Console.WriteLine(value);
        return value;
    }
    
    public ListNode CreateListFromValue(ulong value) {
        if(value == 0) {
            return new ListNode(0);
        }
        
        var valueLeftToEncode = value;
        ListNode head = null;
        ListNode tail = null;
        
        while(valueLeftToEncode > 0) {
            var valueToEncode = Convert.ToInt32(valueLeftToEncode % Base);
            
            var encodedValue = new ListNode(valueToEncode);
            if(head == null) {
                head = encodedValue;
                tail = encodedValue;
            } else {
                tail.next = encodedValue;
                tail = tail.next;
            }
            
            valueLeftToEncode = valueLeftToEncode - (ulong)valueToEncode;
            valueLeftToEncode /= Base;
        }
        
        return head;
    }
}