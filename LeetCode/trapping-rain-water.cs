// This solves the problem in O(n^2) time worst case with
// O(n) extra space required. It performs significantly better if 
// high walls are far apart

public class Solution {
    public int Trap(int[] height) {
        var heights = new List<HeightIndexPair>();
        for(var index = 0; index < height.Length; index++) {
            heights.Add(
                new HeightIndexPair
                {
                    Index = index,
                    Height = height[index]
                });
        }
        var sortedHeights = heights.OrderByDescending(h => h.Height).ToList();

        // hamytodo - enumerable is really slow so may be better to do own
        int[] accountedForReservoirs = Enumerable.Repeat(0, height.Length).ToArray();

        var foundReservoirs = new List<Reservoir>();
        for(var index = 0; index < sortedHeights.Count; index++) {
            var wall = sortedHeights[index];
            foundReservoirs.AddRange(
                PlaceNewWallInAccountedForReservoirArray(
                    accountedForReservoirs,
                    wall.Index
                ));
        }

        var totalVolume = 0;
        for(var index = 0; index < foundReservoirs.Count; index++) {
            totalVolume += FindWaterVolumeInReservoir(
                height,
                foundReservoirs[index]
            );
        }
        return totalVolume;
    }

    public List<Reservoir> PlaceNewWallInAccountedForReservoirArray(
        int[] accountedForReservoirs,
        int index) {
        if(accountedForReservoirs[index] == 1) {
            return new List<Reservoir>();
            // hamy leetCode doesn't like ImmutableList
            // return ImmutableList<Reservoir>.Empty;
        }

        accountedForReservoirs[index] = 1;
        var foundReservoirs = new List<Reservoir>();

        // look ahead if needed (only if we have a forward wall!)
        var requiresForwardLookAhead = false;
        for(var forwardPointer = index + 1;
            forwardPointer < accountedForReservoirs.Length;
            forwardPointer++) {
            if(accountedForReservoirs[forwardPointer] == 1) {
                requiresForwardLookAhead = true;
                break;
            }
        }

        if(requiresForwardLookAhead) {
            for(var forwardPointer = index + 1;
                forwardPointer < accountedForReservoirs.Length;
                forwardPointer++) {
                if(accountedForReservoirs[forwardPointer] == 1) {
                    foundReservoirs.Add(
                        new Reservoir {
                            StartIndex = index,
                            EndIndex = forwardPointer
                        }
                    );
                    break;
                }
                accountedForReservoirs[forwardPointer] = 1;
            }
        }

        // look behind if there's a wall
        var requiresLookBehind = false;
        for(var backPointer = index - 1;
            backPointer >= 0;
            backPointer--) {
            if(accountedForReservoirs[backPointer] == 1) {
                requiresLookBehind = true;
            }
        }

        if(requiresLookBehind) {
            for(var backPointer = index - 1;
            backPointer >= 0;
            backPointer--) {
                if(accountedForReservoirs[backPointer] == 1) {
                    foundReservoirs.Add(
                        new Reservoir {
                            StartIndex = backPointer,
                            EndIndex = index
                        }
                    );
                    break;
                }
                accountedForReservoirs[backPointer] = 1;
            }
        }

        return foundReservoirs;
    }

    public int FindWaterVolumeInReservoir(int[] heights, Reservoir reservoir) {
        // hamy: the maxHeight is the smallest height of the two
        var maxHeight = heights[reservoir.StartIndex] < heights[reservoir.EndIndex]
            ? heights[reservoir.StartIndex]
            : heights[reservoir.EndIndex];
        var width = reservoir.EndIndex - reservoir.StartIndex - 1;

        if(maxHeight <= 0 || width <= 0) {
            return 0;
        }

        var maxVolume = maxHeight * width;

        for(var index = reservoir.StartIndex + 1;
            index < reservoir.EndIndex;
            index++) {
            maxVolume-=heights[index];
        }
        return maxVolume;
    }

    public class Reservoir {
        public int StartIndex { get; set; }
        public int EndIndex { get; set; }
    }

    public class HeightIndexPair {
        public int Index { get; set; }
        public int Height { get; set; }
    }
}