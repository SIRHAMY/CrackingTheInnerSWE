/**
 * Definition for singly-linked list with a random pointer.
 * public class RandomListNode {
 *     public int label;
 *     public RandomListNode next, random;
 *     public RandomListNode(int x) { this.label = x; }
 * };
 */
public class Solution {
    public RandomListNode CopyRandomList(RandomListNode head) {
        return CopyRandomNode(
            new Dictionary<int, RandomListNode>(),
            head
        );
    }

    public RandomListNode CopyRandomNode(
        Dictionary<int,RandomListNode> visited,
        RandomListNode nodeToCopy
    ) {
        if(nodeToCopy == null) {
            return null;
        }

        // if already been to (thus copied) node, return that
        if(visited.ContainsKey(nodeToCopy.label)) {
            return visited[nodeToCopy.label];
        }

        var newNode = new RandomListNode(nodeToCopy.label);
        visited.Add(newNode.label, newNode);

        newNode.random = CopyRandomNode(
            visited,
            nodeToCopy.random);
        newNode.next = CopyRandomNode(
            visited,
            nodeToCopy.next);

        return newNode;
    }
}

// hamy
// Assumption
// * every node has unique label
// Approach O(n) time and space
// * iterate over original list
//  * make copies of the nodes as we go
//  * insert into hashtable by label when we need to make a new one
//  * will likely need to be while .next ! in visited, while .random ! in visited