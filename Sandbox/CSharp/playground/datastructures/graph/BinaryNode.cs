namespace Playground.DataStructures.Graph {
    public class BinaryNode {
        public int Value { get; set; }
        public BinaryNode LeftChild { get; set; }
        public BinaryNode RightChild { get; set; }
    }
}