using System;

namespace Playground.DataStructures.Graph {
    public static class BinaryTreeService {

        // CTCI 4.4
        // HAM: Balanced here means the heights of the two subtrees
        // of any node do not differ by more than one
        public static bool IsBinaryTreeBalanced(BinaryNode root) {
            return IsBinaryTreeBalancedInternal(root).Item1;
        }

        private static Tuple<bool, int> IsBinaryTreeBalancedInternal(BinaryNode root) {
            if(root.LeftChild == null && root.RightChild == null) {
                return Tuple.Create(true, 1);
            }

            Tuple<bool, int> leftResult = Tuple.Create(true, 0);
            if(root.LeftChild != null) {
                leftResult = IsBinaryTreeBalancedInternal(root.LeftChild);
            }

            Tuple<bool, int> rightResult = Tuple.Create(true, 0);            
            if(root.RightChild != null) {
                rightResult = IsBinaryTreeBalancedInternal(root.RightChild);
            }

            var isBalanced = leftResult.Item1
                && rightResult.Item1
                && (Math.Abs(leftResult.Item2 - rightResult.Item2) < 2);
            var maxDepth = Math.Max(leftResult.Item2, rightResult.Item2);
            return Tuple.Create(isBalanced, maxDepth + 1);
        }

        // CTCI 4.5
        public static bool IsBinarySearchTreeValid(BinaryNode root) {
            return IsBinarySearchTreeValidInternal(root);
        }

        // hamy - this is insufficient because we don't do bounds inside subtrees...
        private static bool IsBinarySearchTreeValidInternal(BinaryNode root) {
            if(root.LeftChild != null) {
                if(root.LeftChild.Value > root.Value
                    || !IsBinarySearchTreeValidInternal(root.LeftChild)) {
                    return false;
                }
            }

            if(root.RightChild != null) {
                if(root.RightChild.Value < root.Value
                    || !IsBinarySearchTreeValidInternal(root.RightChild)) {
                    return false;
                }
            }

            return true;
        }
    }
}