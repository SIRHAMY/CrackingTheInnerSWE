namespace Playground.PastProblems {
    // hamy - issues
    // * there's an infinite loop somewhere
    public class BigNumberLongDivision {
        // hamy
        // further improvements
        // * deal with remainders
        // * deal with decimals
        public static string DivideTwoStrings(string dividend, string divisor) {
            // hamy - alternatively, we could do long division but in worst case,
            // it devolves to this subtraction paradigm (cause relying on internal
            // division / modulation would require cast to int and thus still may overflow
            // with sufficiently large numbers)

            return DivideTwoStringsInternal(dividend, divisor, out var _);
        }

        private static string DivideTwoStringsInternal(string dividend, string divisor, out string remainder) {
            // hamy - we could make the quotient add a string operation as well
            var quotient = 0;
            var currentDividend = dividend;
            while(TrySubtractTwoStrings(currentDividend, divisor, out var result)) {
                quotient++;
                currentDividend = result;
            }
            remainder = currentDividend;
            return quotient.ToString();
        }

        // HAM: only deals with positive results
        private static bool TrySubtractTwoStrings(string subtractee, string subtractor, out string result) {
            var workingSubtractee = subtractee;
            var workingResult = "";

            if(subtractee.Length < subtractor.Length) {
                result = subtractee;
                return false;
            }

            var subtractPointer = 0;
            while(subtractPointer < subtractor.Length) {
                var currentSubtractee = int.Parse(workingSubtractee[workingSubtractee.Length - 1 - subtractPointer].ToString());
                var currentSubtractor = int.Parse(subtractor[subtractor.Length - 1 - subtractPointer].ToString());

                if(currentSubtractee < currentSubtractor) {
                    var grabPointer = subtractPointer + 1;
                    while(grabPointer < workingSubtractee.Length
                        && workingSubtractee[grabPointer] == '0') {
                        grabPointer++;
                    }
                    
                    // HAM: This means the orig subtractee was bigger than the
                    // subtractor and we should fail
                    if(grabPointer == workingSubtractee.Length) {
                        result = subtractee;
                        return false;
                    }

                    var grabValue = int.Parse(workingSubtractee[grabPointer].ToString());
                    workingSubtractee = ReplaceStringIndexWithCharacter(
                        workingSubtractee,
                        (grabValue - 1).ToString()[0],
                        grabPointer);
                    grabPointer--;
                    while(grabPointer > subtractPointer) {
                        workingSubtractee = ReplaceStringIndexWithCharacter(
                            workingSubtractee,
                            (grabValue + 10 - 1).ToString()[0],
                            grabPointer);
                        grabPointer--;
                    }
                    currentSubtractee += 10;
                }

                var currentResult = currentSubtractee - currentSubtractor;
                workingResult = currentResult + workingResult;
            }
            result = workingResult;
            return true;
        }

        private static string ReplaceStringIndexWithCharacter(string original, char toPlace, int indexToPlace) {
            return original.Substring(0, indexToPlace) + toPlace + original.Substring(indexToPlace + 1);
        }
    }
}