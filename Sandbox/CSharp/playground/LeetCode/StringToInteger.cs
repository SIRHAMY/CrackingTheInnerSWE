using System;

namespace LeetCode {
    public class StringToInteger8 {
        const int ConversionFailureSentinel = 0;
        public int Run(string str) {
            // parse to first non-whitespace char
            var trimmedString = str.Trim();

            var readPointer = 0;
            var isNegative = false;

            if(readPointer >= trimmedString.Length) {
                return ConversionFailureSentinel;
            }
            if(trimmedString[readPointer] == '-') {
                isNegative = true;
                readPointer++;
            } else if(trimmedString[readPointer] == '+') {
                readPointer++;
            }

            var endPointer = readPointer;
            while(endPointer < trimmedString.Length
                && Char.IsDigit(trimmedString[endPointer])) {
                    endPointer++;
            }

            // check if any numbers at all
            if(endPointer - readPointer == 0) {
                return ConversionFailureSentinel;
            }

            if(
                Int32.TryParse(
                    trimmedString.Substring(readPointer, endPointer - readPointer),
                    out var parsedInt)) {
                return parsedInt
                    * (isNegative
                        ? -1
                        : 1);
            }

            if(isNegative) {
                return Int32.MinValue;
            } else {
                return Int32.MaxValue;
            }
        }
    }
}