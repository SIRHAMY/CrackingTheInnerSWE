using System;
using System.Text;

namespace LeetCode {
    // HAMY
    // * breaks on rowCount = 1 case
    public class ZigZagConversion6 {
        public string Convert(string s, int numRows) {
            var stringBuilder = new StringBuilder();

            for(var initialRow = 0; initialRow < numRows; initialRow++) {
                var directionDown = true;
                var readPtr = initialRow;
                while(readPtr < s.Length) {
                    stringBuilder.Append(s[readPtr]);
                    readPtr += this.GetSpotsToJump(
                        numRows,
                        directionDown
                            ? initialRow
                            : numRows - (initialRow + 1));
                    directionDown = !directionDown;
                }
            }

            return stringBuilder.ToString();
        }

        // HAM: rowNumber here is 1-indexed
        private int GetSpotsToJump(int rowCount, int rowNumber) {
            var rowNumberToUse = rowNumber == rowCount - 1
                ? 0
                : rowNumber;
            return (rowCount * 2) - (2*(rowNumberToUse + 1));
        }
    }
}