using System;
using System.Collections.Generic;

namespace LeetCode {
    public class ValidateBinarySearchTree98 {

        // hamy todo
        // * doesn't handle case where the inputs are at min/max value well
        // ** [-2147483648,-2147483648] - expected: false, was true
        public bool IsValidBST(TreeNode root) {
            if(root == null) {
                return true;
            }
            return this.IsValidBSTInternal(root, int.MaxValue, int.MinValue);
        }

        private bool IsValidBSTInternal(
            TreeNode root,
            int maxInclusive,
            int minInclusive) {

            // check curr node within boundaries
            if(root.val < minInclusive
                || root.val > maxInclusive) {
                return false;
            }

            // check left node within boundaries
            if(root.left != null
                && !this.IsValidBSTInternal(
                        root.left,
                        maxInclusive: root.val - 1,
                        minInclusive: minInclusive
                        )) {
                return false;
            }

            // check right node within boundaries
            if(root.right != null
                && !this.IsValidBSTInternal(
                        root.right,
                        maxInclusive: maxInclusive,
                        minInclusive: root.val + 1
                    )) {
                return false;
            }

            return true;
        }

        public class TreeNode {
            public int val;
            public TreeNode left;
            public TreeNode right;
            public TreeNode(int x) { val = x; }
        }
    }
}