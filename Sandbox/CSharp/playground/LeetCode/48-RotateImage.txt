BCR O(n)

* approach O(n), 1
    * move one px at a time in a rotation

public void Rotate(int[][] matrix) {
    for(var y = 0; y < matrix.GetLength(0) / 2; y++) {
        for(var x = 0; x < matrix.GetLength(0) - 1; x++) {
            // top to right
            var rightValueHolder = matrix[x][matrix.GetLength(0) - 1 - y];
            matrix[x][matrix.GetLength(0) - 1 - y] = matrix[y][x];
            
            // right to bot
            var botValueHolder = matrix[matrix.GetLength(0) - 1 - y][matrix.GetLength(0) - 1 - x];
            matrix[matrix.GetLength(0) - 1 - y][matrix.GetLength(0) - 1 - x] = rightValueHolder;

            // bot to left
            var leftValueHolder = matrix[matrix.GetLength(0) - 1 - x][y];
            matrix[matrix.GetLength(0) - 1 - x][y] = botValueHolder;

            // left to top
            matrix[y][x] = leftValueHolder;
        }
    }
}