using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace LeetCode {
    public class PhoneNumberLetterCombinations17 {
        public IList<string> LetterCombinations(string digits) {
            if(digits.Length == 0) {
                return ImmutableList<string>.Empty;
            }

            var letterCombinations = new List<string>();
            this.FindLetterCombinationsInternal(digits, "", letterCombinations);
            return letterCombinations;
        }

        private void FindLetterCombinationsInternal(
            string digits,
            string built,
            IList<string> letterCombinations) {
            if(digits.Length == built.Length) {
                letterCombinations.Add(built);
                return;
            }

            foreach(var possibility in GetLetterPossibilitiesForNumericalCharacter(digits[built.Length])) {
                this.FindLetterCombinationsInternal(
                    digits,
                    built + possibility,
                    letterCombinations
                );
            }
        }

        private static string[] GetLetterPossibilitiesForNumericalCharacter(char numberCharacter) {
            if(!int.TryParse(numberCharacter.ToString(), out var value)) {
                throw new NotSupportedException("character not a number");
            }

            switch(value) {
                case 0:
                case 1:
                    throw new NotSupportedException("bad value");
                case 2: 
                    return new[] { "a", "b", "c" };
                case 3:
                    return new[] { "d", "e", "f" };
                case 4:
                    return new[] { "g", "h", "i" };
                case 5:
                    return new[] { "j", "k", "l" };
                case 6:
                    return new[] { "m", "n", "o" };
                case 7:
                    return new[] { "p", "q", "r", "s" };
                case 8:
                    return new[] { "t", "u", "v" };
                case 9:
                    return new[] { "w", "x", "y", "z" };
                default:
                    throw new NotSupportedException("Should never get here");
            }
        }
    }

    //test cases
    // * empty string
    // * arbitrary string - 2/3
    // * string that contains bad chars - letter, 0/1
}