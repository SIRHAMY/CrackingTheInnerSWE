using System;
using System.Collections.Generic;

namespace LeetCode {
    public class ValidateBinarySearchTree20 {
        public bool IsValid(string s) {
            var openStack = new Stack<char>();

            for(var i = 0; i < s.Length; i++) {
                var currentCharacter = s[i];

                if(this.IsOpenCharacter(currentCharacter)) {
                    openStack.Push(currentCharacter);
                } else {
                    if(openStack.Count == 0
                        || currentCharacter != this.GetValidClosingCharacterForOpen(openStack.Pop())) {
                        return false;
                    }
                }
            }

            if(openStack.Count != 0) {
                return false;
            }

            return true;
        }

        private bool IsOpenCharacter(char c) {
            switch(c) {
                case '(':
                case '{':
                case '[':
                    return true;
                default:
                    return false;
            }
        }

        private char GetValidClosingCharacterForOpen(char openCharacter) {
            switch(openCharacter) {
                case '(':
                    return ')';
                case '{':
                    return '}';
                case '[':
                    return ']';
                default:
                    throw new InvalidOperationException("Invalid openChar given");
            }
        }
    }
}