using System;

namespace LeetCode {
    public class LongestPalindromicString5 {
        public string LongestPalindrome(string s) {
            var longestPalindrome = string.Empty;
            for(var readPointer = 0; readPointer < s.Length; readPointer++) {
                var foundPalindrome = this.FindLongesPalindromeFromPalindromePointers(
                    readPointer,
                    readPointer,
                    s);
                if(foundPalindrome.Length > longestPalindrome.Length) {
                    longestPalindrome = foundPalindrome;
                }
                if(readPointer < s.Length - 1
                    && s[readPointer] == s[readPointer + 1]) {
                    var foundPalindrome2 = this.FindLongesPalindromeFromPalindromePointers(
                        readPointer,
                        readPointer + 1,
                        s);
                    if(foundPalindrome2.Length > longestPalindrome.Length) {
                        longestPalindrome = foundPalindrome2;
                    }
                }
            }
            return longestPalindrome;
        }

        private string FindLongesPalindromeFromPalindromePointers(
            int lowPointer,
            int highPointer,
            string s
        ) {
            // hamy - throw if lowPointer > highPointer
            // hamy - throw if lowPointer too low / highPointer too high
            while(lowPointer > 0 
            && highPointer < s.Length - 1
            && s[lowPointer - 1] == s[highPointer + 1]) {
                lowPointer--;
                highPointer++;
            }
            return s.Substring(lowPointer, (highPointer - lowPointer) + 1 );
        }

        // testcases
        // * empty string
        // * 1 letter
        // * no palindromes greater than one letter
        // * show it does numbers / symbols
    }
}