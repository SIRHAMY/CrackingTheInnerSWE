using System;
using System.Text;

namespace LeetCode {
    // hamy - wrong because there could be multiple bytes in a row...
    public class Utf8Validation393 {
        public bool ValidUtf8(int[] data) {
            if(data.Length == 0) {
                return false;
            }

            var binaryString = this.GetByteBinaryStringFromInt(data[0]);
            var leadOnesPointer = 0;
            while(leadOnesPointer < binaryString.Length
                && binaryString[leadOnesPointer] != '0') {
                leadOnesPointer++;
            }

            var totalBytes = leadOnesPointer;
            if(totalBytes > 4 || leadOnesPointer == 1) {
                return false;
            }
            if(leadOnesPointer == 0) {
                totalBytes = 1;
            }

            if(data.Length != totalBytes) {
                return false;
            }

            for(var valuePointer = 1; valuePointer < data.Length; valuePointer++) {
                var byteBinary = this.GetByteBinaryStringFromInt(data[valuePointer]);
                if(byteBinary[0] != '1'
                    || byteBinary[1] != '0') {
                    return false;
                }
            }
            return true;
        }

        private string GetByteBinaryStringFromInt(int value) {
            var binary = Convert.ToString(value, 2);
            var binaryLength = binary.Length;
            if(binaryLength == 8) {
                return binary;
            }

            var builder = new StringBuilder();
            for(var bitsLeft = 8 - binaryLength; bitsLeft > 0; bitsLeft--) {
                builder.Append('0');
            }
            builder.Append(binary);
            return builder.ToString();
        }
    }
}