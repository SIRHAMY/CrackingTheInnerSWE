* Brute force O(n^2)
    * for every starting position that is valid, search rest of string for validity
* O(n), n -> 1 if just holding pointers
    * if at some point, more closes than opens - longest can't be on that side
        * use that to start window again
    * iterate over string til find open
        * openCounter = 1, closeCounter = 0
        * while opens >= closes, go
        * closes > opens
            * find best string in 0, pointer (excl) ->is the best!
            * by pulling in butt, save that
            * set iteration pointer to readPointer

public int LongestValidParentheses(string s) {
    var largestFoundParentheses = string.Empty;

    for(var readPointer = 0; readPointer < s.Length - 1; readPointer++) {
        if(s[readPointer] != '(') {
            continue;
        }
        var openCounter = 0;
        var closeCounter = 0;
        var searchPointer = readPointer;
        while(openCounter <= closeCounter
            && searchPointer < s.Length) {
            s[searchPointer] == '('
                ? openCounter++
                : closeCounter++;
        }
        if(openCounter <= closeCounter) {
            var foundParentheses = s.Substring(readPointer, searchPointer);
            if(foundParentheses.Length > largestFoundParentheses.Length) {
                largestFoundParentheses = foundParentheses;
            }
            readPointer = searchPointer;
        // got to end of string, didn't find a terminated valid subset, need to pull int butt for finding
        } else {
            // works, but not as good of O as I'd hoped
        }
    }
}

// ((()()((()