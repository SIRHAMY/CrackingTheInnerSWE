* Do same as regular permutations, but push list into a set
    * bad cause lots of memory
    * good cause works
* 

public IList<IList<int>> PermuteUnique(int[] nums) {
        var potentialIntegersDict = new Dictionary<int, int>();
        foreach(var num in nums) {
            if(potenticalIntegersDict.Contains(num)) {
                potentialIntegersDict.Put(
                    num,
                    potentialIntegersDict[num] + 1
                );
            } else {
                potenticalIntegersDict.Add(
                    num,
                    1
                );
            }
        }
        var allPermutations = new List<IList<int>>();

        if(nums.Count == 0) {
            return allPermutations;
        }

        this.PermuteInternal(
            allPermutations,
            potentialIntegersDict,
            new List<int>()
        );
        return allPermutations;
}

private void PermuteInternal(
    IList<IList<int>> foundPermutations,
    Dictionary<int, int> potentialIntegers,
    IList<int> currentPermutation) {
    if(potentialIntegers.Count == 0) {
        foundPermutations.Add(currentPermutation);
    }

    potentialIntegers.Keys.Distinct().ToList().ForEach(
        value => {
            if(potentialIntegers[value] == 0) {
                continue;
            }
            var dictWithLoweredValue = new Dictionary<int,int>(potentialIntegers);
            dictWithLoweredValue.Put(value, dictWithLoweredValue[value] - 1);
            this.PermuteInternal(
            foundPermutations,
            dictWithLoweredValue,
            currentPermutation.Append(value).ToList()
            );
        });
}