using System;
using System.Collections.Generic;

namespace LeetCode {
    public class BinaryTreeLevelOrderTraversal102 {
        public IList<IList<int>> LevelOrder(TreeNode root) {
            var levelOrderList = new List<IList<int>>();
            if(root == null) {
                return levelOrderList;
            }

            var toVisit = new Queue<TreeNodeWithLevel>();
            toVisit.Enqueue(new TreeNodeWithLevel {
                Level = 0,
                TreeNode = root
            });

            while(toVisit.Count > 0) {
                var visiting = toVisit.Dequeue();
                if(levelOrderList.Count <= visiting.Level) {
                    levelOrderList.Add(new List<int>());
                }
                levelOrderList[visiting.Level].Add(visiting.TreeNode.val);

                if(visiting.TreeNode.left != null) {
                    toVisit.Enqueue(new TreeNodeWithLevel {
                        Level = visiting.Level + 1,
                        TreeNode = visiting.TreeNode.left
                    });
                }

                if(visiting.TreeNode.right != null) {
                    toVisit.Enqueue(new TreeNodeWithLevel {
                        Level = visiting.Level + 1,
                        TreeNode = visiting.TreeNode.right
                    });
                }
            }

            return levelOrderList;
        }

        public class TreeNode {
            public int val;
            public TreeNode left;
            public TreeNode right;
            public TreeNode(int x) { val = x; }
        }

        public class TreeNodeWithLevel {
            public int Level { get; set; }
            public TreeNode TreeNode { get; set; }
        }
    }
}