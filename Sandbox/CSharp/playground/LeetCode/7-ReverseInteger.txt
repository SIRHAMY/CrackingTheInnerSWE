public int Reverse(int x) {
    var integerAsString = x.ToString();

    var isNegative = false;
    if(integerAsString[0] == '-') {
        isNegative = true;
        integerAsString = integerAsString.Substring(1);
    }
    integerAsString = new string(integerAsString.Reverse().ToArray());

    if(!int.TryParse(integerAsString, out var reversedInteger)) {
        return 0;
    }

    if(isNegative) {
        return -1 * reversedInteger;
    }
    return reversedInteger;
}

//
* string to int O(n), n
    * check if negative
    * use language int to string
    * reverse string
    * try parse int
    * return 0 if fail
* int to int
    * do with modulus of base 10 pow

TestCases
* 0
* -1
* 1
* 120
* v big
* v small