using NUnit.Framework;
using Playground.DataStructures.Graph;

namespace Tests
{
    public class Tests
    {
        private BinaryNode _balancedBinarySearchTreeRoot;
        private BinaryNode _unBalancedBinarySearchTreeRoot;

        [SetUp]
        public void Setup()
        {
            var balancedTreeNodeLeft = new BinaryNode {
                Value = 2
            };

            var balancedTreeNodeRight = new BinaryNode {
                Value = 7
            };

            this._balancedBinarySearchTreeRoot = new BinaryNode {
                Value = 5,
                LeftChild = balancedTreeNodeLeft,
                RightChild = balancedTreeNodeRight
            };

            var unbalancedTreeNodeLeft = new BinaryNode {
                Value = 2
            };

            var unbalancedTreeNodeRightLeftRight = new BinaryNode {
                Value = 5
            };

            var unbalancedTreeNodeRightLeft = new BinaryNode {
                Value = 6,
                RightChild = unbalancedTreeNodeRightLeftRight
            };

            var unbalancedTreeNodeRight = new BinaryNode {
                Value = 7,
                LeftChild = unbalancedTreeNodeRightLeft
            };

            // hamy - this isn't a correct binary search tree 
            this._unBalancedBinarySearchTreeRoot = new BinaryNode {
                Value = 4,
                LeftChild = unbalancedTreeNodeLeft,
                RightChild = unbalancedTreeNodeRight
            };
        }

        [Test]
        public void TestIsBinaryTreeBalanced()
        {
            Assert.IsTrue(BinaryTreeService.IsBinaryTreeBalanced(this._balancedBinarySearchTreeRoot));
            Assert.IsFalse(BinaryTreeService.IsBinaryTreeBalanced(this._unBalancedBinarySearchTreeRoot));
        }

        [Test]
        public void TestIsBinarySearchTreeValid() {
            Assert.IsTrue(BinaryTreeService.IsBinarySearchTreeValid(this._balancedBinarySearchTreeRoot));
            Assert.IsTrue(BinaryTreeService.IsBinarySearchTreeValid(this._unBalancedBinarySearchTreeRoot));

            var invalidTreeNodeRight = new BinaryNode {
                Value = 0
            };

            var invalidTreeNodeRoot = new BinaryNode {
                Value = 1,
                RightChild = invalidTreeNodeRight
            };
            Assert.IsFalse(BinaryTreeService.IsBinarySearchTreeValid(invalidTreeNodeRoot));
        }
    }
}