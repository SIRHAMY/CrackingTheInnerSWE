# Getting started

## Creating a project 

cargo new PROJECTNAME

--bin if binary, --lib if not (library)
--vcs none if don't want a git repo inititalized, defaults to doing it

## Building a project

`cargo build`

`cargo build --release` to build with optimizations (compiles slower but code runs faster)

## Running a project

`cargo run`