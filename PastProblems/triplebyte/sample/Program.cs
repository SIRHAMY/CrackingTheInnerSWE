﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace sample
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("type stuff!");
            // string[] consoleInput = Console.ReadLine().Split(' ');
            // List<string> consoleInputList = new List<string>(consoleInput);
            // Console.WriteLine(string.Join(", ", consoleInputList.Where(c => c.Contains("ha"))));

            var myTicTacToeBoard = new TicTacToeBoard();
            myTicTacToeBoard.PrintBoard();

            myTicTacToeBoard.AddToken(TokenType.Player, 1, 1);
            myTicTacToeBoard.PrintBoard();

        }
    }

    class TicTacToeBoard {
        public List<List<TokenType>> BoardLocations;
        const int BoardSize = 3;

        public TicTacToeBoard() {
            List<List<TokenType>> initialBoardLocations = new List<List<TokenType>>();

            // todo switch
            for(var y = 0; y < BoardSize; y++) {
                var row = new List<TokenType>();
                for(var x = 0; x < BoardSize; x++) {
                    row.Add(TokenType.None);
                }
                Console.WriteLine($"row size: {row.Count}");
                initialBoardLocations.Add(row);
            }
            Console.WriteLine($"initialBoardLocatoins size: {initialBoardLocations.Count}");
            BoardLocations = initialBoardLocations;
        }

        public void PrintBoard() {
            Console.WriteLine($"BoardLocations size: {BoardLocations.Count}");
            for(var y = 0; y < BoardSize; y++) {
                var rowBoardLocations = BoardLocations[y];
                var thingToPrint = $"{getTokenPrint(rowBoardLocations[0])}|{getTokenPrint(rowBoardLocations[1])}|{getTokenPrint(rowBoardLocations[2])}";
                Console.WriteLine(thingToPrint);
            }
        }

        public void AddToken(TokenType player, int x, int y) {
            // todo validations: make sure its tokenType.None

            BoardLocations[y][x] = player;
        }

        private string getTokenPrint(TokenType tokenType) {
            switch(tokenType) {
                case TokenType.Player:
                    return "X";
                case TokenType.Ai:
                    return "O";
                case TokenType.None:
                    return "-";
                default:
                    return "notAThing";
            }
        }

        // todo enum for player
    }

    public enum TokenType {
        Player,
        Ai,
        None
    };
}
